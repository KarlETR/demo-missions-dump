call compile preprocessfilelinenumbers "TOV_CP_framework\TOV_CP_functions.sqf";

[getmarkerPos "s", getmarkerPos "e", getmarkerPos "cp" ] execVM "TOV_CP_framework\TOV_CP_spawner.sqf";

execVM "ambiance.sqf";

[] spawn {
	_CP_pos=getmarkerPos "cp";
	sleep 300;
	while {a} do {
		sleep 300;
		[
			hint format["Checkpoint summary:\n\nChecked Vehicles : %1\nErrors : %2", TOV_checked_vehicles, TOV_checked_vehicles_error]
		] remoteExec ["hint", allPlayers select {_x distance _CP_pos < 60}];
	};
};

{_x setMarkerAlpha 0} foreach ["s","e","cp"]; 