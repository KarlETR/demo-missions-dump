
TOV_animate_market = {
	_eligible_anim=["Acts_CivilIdle_1", "Acts_CivilIdle_2", "Acts_CivilTalking_1", "Acts_CivilTalking_2", "HubStandingUA_idle2", "HubStandingUA_idle3", "HubStandingUB_idle1", "HubStandingUB_idle2", "HubStandingUB_idle3", "HubStandingUC_idle1", "HubStandingUC_idle3", "HubStandingUC_idle2"];
	
	params ["_unit", ["_anim", selectRandom _eligible_anim], ["_delay",0]];
	
	sleep _delay;
	
	if (local _unit) then { 
	    _unit disableAI "MOVE";
	    sleep random 2;
	    _unit switchMove _anim;
		_unit setVariable ["_tov_anim",_anim];
		_evHandle = _unit addEventHandler [ "AnimDone", {
			params[ "_unit", "_anim" ];
			
			_tov_anim=_unit getVariable "_tov_anim";
			if ( _anim == _tov_anim) then {
				_unit switchMove _tov_anim;
			};
		}];
		
	    waitUntil {sleep 2; (behaviour _unit == "COMBAT") or (!(alive _unit))};
		_unit enableAI "MOVE";
	    _unit removeEventHandler ["AnimDone", _evHandle];
		_unit playMove "";
		
    };
};


/*
//Init field for units
this disableAI "MOVE";
this spawn {
		sleep 2;
		[_this, "Acts_AidlPercMstpSloWWrflDnon_warmup_4_loop"] spawn TOV_animate_market;
	};
*/

[] spawn {
	marketCivs=[];
	
	_civs= ["CUP_C_TK_Man"] call TOV_find_civs_classnames;

	for "_i" from 1 to 30 do {
		_dude=createAgent [selectRandom _civs, market_marker call BIS_fnc_randomPosTrigger, [], 0, "NONE"];
		_dude setDir random 360;
		[_dude] spawn TOV_animate_market;
		marketCivs=marketCivs + [_dude];
		//sleep random 0.1;
	};
};


TOV_play_ambient_sounds = {
	params [["_timer",60], ["_init_loops", 10]];
	
	TOV_Sound=True;
	
	for "_i" from 1 to _init_loops do {
		_soundPos= market_marker call BIS_fnc_randomPosTrigger ;
		_soundFile=format[getMissionPath "sound\sfxsound%1.ogg", 1+ floor (random 8)];
		playSound3D [_soundFile, _soundPos, False, _soundPos, 1];	
		sleep random 10;
	};
	
	while {TOV_Sound} do {
		_soundPos= market_marker call BIS_fnc_randomPosTrigger ;
		_soundFile=format[getMissionPath "sound\sfxsound%1.ogg", 1+ floor (random 8)];
		playSound3D [_soundFile, _soundPos, False, _soundPos, 1];	
		sleep random 30;
	};		
};

[] spawn TOV_play_ambient_sounds;