cutText ["", "BLACK FADED", 30, true, true];
0 fadeSound 0;

_camera = "camera" camCreate [0,0,0];
_camera cameraEffect ["Internal","back"];
showCinemaBorder false;
playMusic "narcos";




_camera camPrepareTarget [3824.66,11636.5,0];
_camera camPreparePos [8256.65,11234.5,6.24405];
_camera camPrepareFOV 0.75;
//setAperture 0;
//_camera setDir 275.184;
//[_camera, 1.17641, 0] call BIS_fnc_setPitchBank;
_camera camCommitPrepared 0;
//waitUntil {camCommitted _camera};


sleep 1;
["<t font='PuristaMedium' size = '2'>A script showcase mission</t>",-1,-1,2000,0.5,0] spawn tov_fnc_dynamicText; 
sleep 4;
cutText ["", "BLACK IN", 0.5, true, true];


_camera camPrepareTarget [3824.88,11639,0];
_camera camPreparePos [8256.87,11236.9,5.9931];
_camera camPrepareFOV 0.75;
//setAperture 0;
//_camera setDir 275.184;
//[_camera, 1.17641, 0] call BIS_fnc_setPitchBank;
_camera camCommitPrepared 20;
["<t font='PuristaMedium' size = '1.5'>Arma checkpoint operations</t>",-0.2,0.1,1000,0.5,0] spawn tov_fnc_dynamicText; 
sleep 3;
//waitUntil {camCommitted _camera};




//Jets
["<t font='PuristaLight' size = '1.5'></t>",-0.2,0.1,1000,0.5,0] spawn tov_fnc_dynamicText; 
_camera camPrepareTarget [5813.58,11378.9,0];
_camera camPreparePos [5800.35,11368,60.3236];
_camera camPrepareFOV 0.75;
//setAperture 0;
//_camera setDir 50.5855;
//[_camera, -74.1535, 0] call BIS_fnc_setPitchBank;
_camera camCommitPrepared 0;
//waitUntil {camCommitted _camera};
[
	parseText "<t size='2'>Russia to strenghten its military engagement in support of Takistani Government</t>",
	parseText "Weather coming up hot and steamy - Takistan shelled again from border area - Crédit Agricole to takeover rival bank - "
] spawn BIS_fnc_AAN;


_camera camPrepareTarget [5855.75,11413.5,0];
_camera camPreparePos [5842.52,11402.7,60.3236];
_camera camPrepareFOV 0.75;
//setAperture 0;
//_camera setDir 50.5855;
//[_camera, -74.1535, 0] call BIS_fnc_setPitchBank;
_camera camCommitPrepared 20;
sleep 7;
(uiNamespace getVariable "BIS_AAN") closeDisplay 1;
//waitUntil {camCommitted _camera};




//Feruz


_camera camPrepareTarget [5400.67,6291.99,0];
_camera camPreparePos [5429.63,6316.54,1.93102];
_camera camPrepareFOV 0.42;
//setAperture 0;
//_camera setDir 229.697;
//[_camera, -3.388, 0] call BIS_fnc_setPitchBank;
_camera camCommitPrepared 0;
//waitUntil {camCommitted _camera};
["<t font='PuristaMedium' size = '1.5'>You are taking part in peacekeeping operations<br/> in the Feruz Abad region</t>",0.3,0.7,2000,0.5,0] spawn tov_fnc_dynamicText; 


_camera camPrepareTarget [5400.67,6291.99,0];
_camera camPreparePos [5429.63,6316.54,1.93102];
_camera camPrepareFOV 0.35;
//setAperture 0;
//_camera setDir 229.697;
//[_camera, -3.388, 0] call BIS_fnc_setPitchBank;
_camera camCommitPrepared 20;
sleep 7;
//waitUntil {camCommitted _camera};



//market

_camera camPrepareTarget [5949.14,7356.24,0];
_camera camPreparePos [5943.52,7372.26,1.30177];
_camera camPrepareFOV 0.75;
//setAperture 0;
//_camera setDir 160.654;
//[_camera, -4.44699, 0] call BIS_fnc_setPitchBank;
_camera camCommitPrepared 0;
//waitUntil {camCommitted _camera};
["<t font='PuristaMedium' size = '1'>To limit the risk of incidents in the upcoming annual bazaar,<br/> your assignment is to man a checkpoint in Falar</t>",-0.3,0.7,2000,0.5,0] spawn tov_fnc_dynamicText; 


_camera camPrepareTarget [5949.99,7356.54,0];
_camera camPreparePos [5944.37,7372.56,1.31017];
_camera camPrepareFOV 0.75;
//setAperture 0;
//_camera setDir 160.654;
//[_camera, -4.44699, 0] call BIS_fnc_setPitchBank;
_camera camCommitPrepared 20;
sleep 7;
//waitUntil {camCommitted _camera};




//police

_camera camPrepareTarget [9023.32,4643.31,0];
_camera camPreparePos [5978.98,7382.86,0.170013];
_camera camPrepareFOV 0.7;
//setAperture 0;
//_camera setDir 131.983;
//[_camera, 23.0587, 0] call BIS_fnc_setPitchBank;
_camera camCommitPrepared 0;
//waitUntil {camCommitted _camera};
["<t font='PuristaMedium' size = '1'>Check IDs, license plates,<br/> search for explosives and contraband</t>",0.5,0.7,2000,0.5,0] spawn tov_fnc_dynamicText; 


_camera camPrepareTarget [9022.92,4642.87,0];
_camera camPreparePos [5978.58,7382.42,0.170013];
_camera camPrepareFOV 0.7;
//setAperture 0;
//_camera setDir 131.983;
//[_camera, 23.0587, 0] call BIS_fnc_setPitchBank;
_camera camCommitPrepared 20;
sleep 7;
//waitUntil {camCommitted _camera};



//stop

_camera camPrepareTarget [5963.06,7490.72,0];
_camera camPreparePos [5956.91,7459.44,0.854149];
_camera camPrepareFOV 0.7;
//setAperture 0;
//_camera setDir 11.1261;
//[_camera, -5.07755, 0] call BIS_fnc_setPitchBank;
_camera camCommitPrepared 0;
//waitUntil {camCommitted _camera};
["<t font='PuristaMedium' size = '1'>Use ace interaction to check passengers documents<br/>forbidden items might be in cargo, hidden in the compartment<br/>or under the vehicle</t>",0.2,0.7,2000,0.5,0] spawn tov_fnc_dynamicText; 


_camera camPrepareTarget [5963.59,7490.31,0];
_camera camPreparePos [5957.5,7459.32,0.833481];
_camera camPrepareFOV 0.7;
//setAperture 0;
//_camera setDir 11.1261;
//[_camera, -5.07755, 0] call BIS_fnc_setPitchBank;
_camera camCommitPrepared 20;
sleep 14;
//waitUntil {camCommitted _camera};



//CP

_camera camPrepareTarget [5942.74,7415.52,0];
_camera camPreparePos [5936.98,7424.75,0.823639];
_camera camPrepareFOV 0.57;
//setAperture 0;
//_camera setDir 148.023;
//[_camera, -3.78679, 0] call BIS_fnc_setPitchBank;
_camera camCommitPrepared 0;
//waitUntil {camCommitted _camera};
["<t font='PuristaMedium' size = '1'>You can use the ace ''hold gesture'' to order vehicles to stop<br/>and the ''move forward'' gesture to release them.<br/>So set your keybinds !</t>",-0.2,0.7,2000,0.5,0] spawn tov_fnc_dynamicText; 

_camera camPrepareTarget [5943.38,7416,0];
_camera camPreparePos [5937.66,7425.17,0.82901];
_camera camPrepareFOV 0.4;
//setAperture 0;
//_camera setDir 148.023;
//[_camera, -3.78679, 0] call BIS_fnc_setPitchBank;
_camera camCommitPrepared 20;
sleep 10;
//waitUntil {camCommitted _camera};


cutText ["", "BLACK OUT", 1, true, true];
["<t font='PuristaMedium' size = '1'></t>",0,0.7,2000,0.5,0] spawn tov_fnc_dynamicText; 
//{deleteVehicle _x} forEach _trash;
2 fadeSound 1;
20 fadeMusic 0;
sleep 5;
cutText ["", "BLACK IN", 2, true, true];
_camera cameraEffect ["Terminate","back"];

sleep 3;
[["Checkpoint","Checkpoint_h"]] call BIS_fnc_advHint;

sleep 30;
playMusic "";
0 fadeMusic 1;


