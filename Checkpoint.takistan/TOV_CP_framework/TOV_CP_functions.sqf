



/////////////// Tools

TOV_find_civs_classnames = {
	params [["_str","C_Man"]];
	_civs_classnames = [];
	_cfg = (configFile >> "CfgVehicles");

	for "_i" from 0 to ((count _cfg) -1) do 
		{
		if (isClass ((_cfg select _i) ) ) then 
			{
			_cfgName = configName (_cfg select _i);
			if ( (_cfgName isKindOf "camanbase") && {getNumber ((_cfg select _i) >> "scope") == 2} && {[_str,str _cfgname] call BIS_fnc_inString}) then 
				{
				_civs_classnames set [count _civs_classnames,_cfgname];
				};
			};
		};
	_civs_classnames
};


///////////////GESTURES

[{	
	TOV_eligible_to_gesture= {
		_nearVehicles = nearestObjects [player, ["CAManBase", "Car", "Truck" , "Tank"], 30] - [_veh];
		_nearVehicles = _nearVehicles select { [getpos player, getdir player, 90, getpos _x] call BIS_fnc_inAngleSector};
		
		_nearVehicles = _nearVehicles select {(_x getVariable ["TOV_CP_object", False]) || (side _x == CIVILIAN)};
		
		if (count _nearVehicles >0) then { 
			_nearVehicles resize 1;
			_nearVehicles=_nearVehicles + [driver (_nearVehicles#0)];
		};

		_nearVehicles;
	};

	if !(isNil "TOV_ges_test") then {player removeEventHandler ["GestureChanged", TOV_ges_test];}; 

	TOV_ges_test = player addEventHandler ["GestureChanged", { 
		params ["_unit", "_gesture"];
		
		//hint str _gesture; 
		
		switch (True) do
		{
			case (["hold", _gesture, False] call BIS_fnc_inString) : { 
				//hint "hold"; 
				{
					_x setVariable ["TOV_manually_controlled", True, True];
					[_x, "MOVE"] remoteExec ["disableAI", _x];				
				} forEach (call TOV_eligible_to_gesture);
				//hint str (call TOV_eligible_to_gesture);
				};
				
			case (["forward", _gesture, False] call BIS_fnc_inString) : {
				//hint "forward" ;
				{	
					[_x] spawn {
						params ["_myTarget"];
						_myTarget setVariable ["TOV_manually_controlled", True, True];
						if (_myTarget getVariable ["TOV_manually_controlled_pending", false]) exitwith {True} ;
						_myTarget setVariable ["TOV_manually_controlled_pending", True, True];
						
						[_myTarget, "MOVE"] remoteExec ["enableAI", _myTarget];
						_myTarget limitSpeed 5;
						sleep 3;
						
						if !(_myTarget getVariable ["TOV_manually_controlled_pending", false]) exitwith {True} ;
						
						_myTarget setVariable ["TOV_manually_controlled_pending", False, True];
						_myTarget setVariable ["TOV_manually_controlled", False, True];
						_myTarget limitSpeed -1;
					};
				} forEach (call TOV_eligible_to_gesture);
				//hint str (call TOV_eligible_to_gesture);			
				};
			default { };
		};

	}];

}] remoteExec ["call", 0, True];



////////////////PAPERS PLEASE

TOV_checked_vehicles=0;
TOV_checked_vehicles_error=0;
TOV_hint_conformity = {
	params ["_veh", "_CP_pos"];
	//TOV_enable_hint_conformity=True; [player, getmarkerPos "cp"] spawn TOV_hint_conformity
	
	if !(TOV_enable_hint_conformity) exitWith {False};
	
	waitUntil {
			sleep 1;
			(isNull _veh) ||
			(_veh distance _CP_pos < 50)
		};
	if !(TOV_enable_hint_conformity) exitWith {False};
	
	waitUntil {
			sleep 1;
			(isNull _veh) ||
			(_veh distance _CP_pos > 50)
		};
	if !(TOV_enable_hint_conformity) exitWith {False};
	
	_summary=[];
	{_summary append (_x getVariable ["TOV_CP_fraud",[]])} forEach crew _veh + [_veh];
	_summary = _summary arrayIntersect _summary;
	
	if (count _summary == 0) then { 
		_summary = "No mistake on vehicle";
	} else {
		_summary = format ["The vehicle should have been detained !\nReasons : \n%1", _summary];
		TOV_checked_vehicles_error=TOV_checked_vehicles_error+1;
	};
	
	TOV_checked_vehicles=TOV_checked_vehicles+1;
	if ({isPlayer _x} count (crew _veh)>0) exitWith {True};
	[_summary] remoteExec ["hint", allPlayers select {_x distance _CP_pos < 60}];
};


TOV_initialize_unit_info = {
	//initializes both unit and vehicle
	params ["_myUnit", ["_chanceOfFraud", 0.05]];
	//[player] call TOV_initialize_unit_info;
	//player getVariable "TOV_CP_name"
	
	////////Name
	_T_name='';
	if (random 1 > _chanceOfFraud) then {
		_T_name= name _myUnit;
	} else {
		_other= createAgent [typeOf _myUnit, getpos _myUnit, [], 0, 'NONE'];
		_T_name= name _other;
		deleteVehicle _other;
		
		_fraud=_myUnit getVariable ["TOV_CP_fraud",[]];
		_myUnit setVariable ["TOV_CP_fraud", _fraud + ["Wrong Name"] , True];
		_chanceOfFraud=_chanceOfFraud/2;
	};
	_myUnit setVariable ["TOV_CP_name", _T_name , True];


	////////License plate (only if driver)
	_T_license_plate="";
	_veh=vehicle _myUnit;
	if ((_myUnit != vehicle _myUnit) && (_myUnit == driver _veh)) then {
		if (random 1 > _chanceOfFraud) then {
			_T_license_plate= getPlateNumber _veh;
		} else {
			_other= createVehicle [typeOf _veh, _veh, [], 100, 'NONE'];
			_T_license_plate = getPlateNumber _other;
			deleteVehicle _other;
			
			_T_license_plate= _T_license_plate;
			
			_fraud=_myUnit getVariable ["TOV_CP_fraud",[]];
			_myUnit setVariable ["TOV_CP_fraud", _fraud + ["Wrong license plate"] , True];
			_chanceOfFraud=_chanceOfFraud/2;
		};
		_T_license_plate="\nPlate n°: " + _T_license_plate;	
	};
	_myUnit setVariable ["TOV_CP_license_plate", _T_license_plate , True];


	///////Explosives attached
	if (random 1 < _chanceOfFraud) then {
		_veh=vehicle _myUnit;
		
		_spawnSelections=(selectionNames _veh) + (_veh selectionNames "HitPoints");
		_spawnSelections =_spawnSelections select {["cargo.", _x] call BIS_fnc_inString || ["_steering", _x] call BIS_fnc_inString || ["passenger", _x] call BIS_fnc_inString};

		_mySelection="";
		_myPosition=[0,0,0];
		for "_i" from 0 to  100 do {
			//failsafe to not swpawn explosive for misconfigured selections
			_mySelection = selectRandom _spawnSelections;
			_myPosition = _veh selectionPosition _mySelection;
			if ((_myPosition#2 < 0.5) && !(_myPosition isEqualTo [0,0,0])) then {break ;};
		};  

		_explosive = "DemoCharge_Remote_Ammo" createVehicle position _veh;
		[_explosive, true] remoteExec ["ace_dragging_fnc_setCarryable",0];
		_explosive attachTo [_veh, _myPosition, _mySelection, True];
		_explosive setVectorDirAndUp [[random 1, random 1, random 1], [random 1, random 1, random 1]];

		//Debug
		//hint (str _mySelection + str _myPosition); 
		//deleteVehicle deb;
		//deb= "Sign_Sphere25cm_F" createVehicle position _veh;
		//deb attachTo [_veh, _myPosition, _mySelection, True];	
		
		_fraud=_myUnit getVariable ["TOV_CP_fraud",[]];
		_chanceOfFraud=_chanceOfFraud/2;
		_myUnit setVariable ["TOV_CP_fraud", _fraud + ["Explosives hidden"] , True];
	};
	

	///////Contraband in cargo
	if (random 1 < _chanceOfFraud) then {
		_veh=vehicle _myUnit;
		_contra_item= selectRandom TOV_CP_contraband;
		
		if ((random 1<0.5) && TOV_ACE_enabled) then {
			_contra_item = selectRandom TOV_CP_contraband_ACE;
			_contra_item = createVehicle [_contra_item, _veh];
			[_contra_item, 1] call ace_cargo_fnc_setSize;
			[_contra_item, _veh, True] call ace_cargo_fnc_loadItem; 
		} else {
			_veh addItemCargoGlobal [_contra_item, floor random [10,100,500]];	
		};			
		
		_fraud=_myUnit getVariable ["TOV_CP_fraud",[]];
		_chanceOfFraud=_chanceOfFraud/2;
		_myUnit setVariable ["TOV_CP_fraud", _fraud + ["Contraband in cargo"] , True];
	};
		
	//////Explosives in cargo
	if (random 1 < _chanceOfFraud) then {
		_veh=vehicle _myUnit;
		_contra_item= selectRandom TOV_CP_explosives;
		
		if ((random 1<0.5) && TOV_ACE_enabled) then {
			_contra_item = selectRandom TOV_CP_explosives_ace;
			_contra_item = createVehicle [_contra_item, _veh];
			[_contra_item, 1] call ace_cargo_fnc_setSize;
			[_contra_item, _veh, True] call ace_cargo_fnc_loadItem; 
		} else {
			_veh addItemCargoGlobal [_contra_item, floor random [10,100,500]];
		};	
		
		_fraud=_myUnit getVariable ["TOV_CP_fraud",[]];
		_chanceOfFraud=_chanceOfFraud/2;
		_myUnit setVariable ["TOV_CP_fraud", _fraud + ["Explosives in cargo"] , True];
	};
	
	///////Nervousness
	_nervousness_chance=0.2;
	if (count (_myUnit getVariable ["TOV_CP_fraud",[]]) > 0) then {_nervousness_chance=1-_nervousness_chance};
		
	if (random 1 < _nervousness_chance) then { 
		_myUnit setVariable ["TOV_CP_nervous", "This person is nervous\n\n" , True];
	} else {
		_myUnit setVariable ["TOV_CP_nervous", "" , True];
	};
	
	
	_myUnit setVariable ["TOV_CP_object", True, True];
};


TOV_add_cp_interaction = {
	params ["_veh"];
	//must be remote exec
	
	[
		[_veh],
		{
			params ["_veh"];
			
			sleep 2;
			
			_action= {
				_txt = format ["%1%2%3", 
					_target getVariable "TOV_CP_nervous",
					_target getVariable "TOV_CP_name",
					_target getVariable "TOV_CP_license_plate"
					];
				hint _txt;
			};
			
			//ID action
			_action = ["ID","View ID","",_action, {true}] call ace_interact_menu_fnc_createAction; 
			
			_action_unload = ["TOV_unload","Get out","",{[_target] call ace_common_fnc_unloadPerson}, {true}] call ace_interact_menu_fnc_createAction;
			
			_action_getin = ["TOV_getin","Get back in","",{_veh=_target getVariable ["TOV_assigned_Vehicle", _target]; _target setDestination [_veh,"LEADER PLANNED", true]; if (_target distance _veh <5) then {_target moveInCargo _veh}; _target assignAsCargo _veh; [_target] orderGetIn true; }, {true}] call ace_interact_menu_fnc_createAction;
			
			//Add to passengers
			{
				
				//Add get out if agent and get in
				if (isAgent teamMember _x) then {
					[_veh, 0, ["ACE_MainActions", "ACE_Passengers", str _x], _action_unload] call ace_interact_menu_fnc_addActionToObject;
					
					//add to dismounted passengers
					[_x, 0, ["ACE_MainActions"], _action_getin] call ace_interact_menu_fnc_addActionToObject;	
				};
				
				//add to boarded passengers
				[_veh, 0, ["ACE_MainActions", "ACE_Passengers", str _x], _action] call ace_interact_menu_fnc_addActionToObject;
				
				//add to dismounted passengers
				[_x, 0, ["ACE_MainActions"], _action] call ace_interact_menu_fnc_addActionToObject;	
				
			} forEach crew _veh;
			
			
			//Add to vehicle	
			
		}
		
	] remoteExec ["spawn", 0, True];
	
};


TOV_add_cp_interaction_old = {
	params ["_veh"];
	
	_action={
		_txt = format ["%1%2", 
			_target getVariable "TOV_CP_nervous",
			_target getVariable "TOV_CP_name"
			];
		hint _txt;
	};
	
	//ID action
	_action = ["ID","View ID","",_action, {true}] call ace_interact_menu_fnc_createAction; 
	
	//Add to passengers
	{
		//add to boarded passengers
		[_veh, 0, ["ACE_MainActions", "ACE_Passengers", str _x], _action] remoteExec ["ace_interact_menu_fnc_addActionToObject", 0, True];

		//add to dismounted passengers
		[_x, 0, ["ACE_MainActions"], _action] remoteExec ["ace_interact_menu_fnc_addActionToObject", 0, True];	
		
	} forEach crew _veh;
	
	
	//Add to vehicle
	
};



//////////////////////TRAFFIC MANAGER

//Front speed manager
TOV_follow_road_at_CP = {
	params ["_veh", "_currentSpawnPoint", "_currentDestinationPoint", "_CP_pos", ["_range", 150]];
	
	driver _veh disableAI "ALL" ;
	{driver _veh enableAI _x} forEach ["ANIM", "MOVE", "PATH", "LIGHTS"];
	
	_start= getpos _veh;
	
	//because wp is global, have to wait until finished building in case multiple CP
	if 	(isNil "TOV_wp_roads") then {
		TOV_wp_roads = [];
	} else {
		waitUntil {sleep 0.1; (TOV_wp_roads isEqualTo [])};
	};
	
	private _obj = (calculatePath ["wheeled_APC", "safe", _start, _currentDestinationPoint]) addEventHandler ["PathCalculated",{
		_temp_wp=[];
		{
			//_mrk = createMarker ["marker" + str _forEachIndex, _x];
			//_mrk setMarkerType "mil_dot";
			//_mrk setMarkerText str _forEachIndex;

			_temp_wp pushBack _x;
		} forEach (_this#1);
		
		if (count _temp_wp < 10) exitWith {False};
		
		TOV_wp_roads = TOV_wp_roads + _temp_wp;
		
	}]; 

	doStop _veh;
	waitUntil {sleep 0.1; !(TOV_wp_roads isEqualTo [])};
	
	_current_wp = +TOV_wp_roads;
	TOV_wp_roads = [];
	
	_current_wp deleteRange [0,6];

	
	//barycenter trick to smooth path - sometimes on junctions arma gets mad
	_new_arr=[];
	for "_i" from 0 to (count _current_wp) -4 do {
		_cur= _current_wp#_i vectorAdd _current_wp#(_i+1) vectorAdd _current_wp#(_i+2);
		_cur= _cur vectorMultiply 1/3;
		//no smoothing if close
		if (_cur distance _current_wp#(_i+1) < 3) then {_cur=_current_wp#(_i+1)};
		
		_nroad=[_cur, 15] call BIS_fnc_nearestRoad;
		if !(isNull _nroad) then {_cur=getPos _nroad}; //else {continue};
	
		if (isOnRoad [_cur#0,_cur#1]) then {_new_arr append [_cur]};
		
		//_new_arr append [_cur];
	};
	_new_arr = _new_arr arrayIntersect _new_arr;
	
	_veh setDir (_veh getDir _new_arr#0);
	//_veh setDriveOnPath TOV_wp_roads;
	_veh setDriveOnPath _new_arr;
	
	//TOV_wp_roads = [];
};


TOV_speed_manager= {
	params ["_veh",["_maxSpeed", 60]];
	
	while {!isNull _veh} do {
	
		_nearVehicles = nearestObjects [_veh, ["CAManBase", "Car", "Truck" , "Tank"], 100] - [_veh];
		
		// DENSITY SECTION
		//100 few - 500 is town
		_nearDensity = count (nearestTerrainObjects [_veh, [], 100]);
		_densityMaxSpeed = _maxSpeed * linearConversion [100, 600, _nearDensity, 1, 0.4, true];

		//ALLROUND SECTION
		_nearVehiclesAllRound = _nearVehicles select { [getpos _veh, getdir _veh, 270, getpos _x] call BIS_fnc_inAngleSector };

		_nearVehiclesAllRound = _nearVehicles apply { _veh distance _x } ;
		_nearVehiclesAllRound= _nearVehiclesAllRound + [1000];
		//was 5 0.1
		_allRoundMaxSpeed = _maxSpeed * linearConversion [0, 50, selectMin _nearVehiclesAllRound, 0, 1, true];


		//FRONT SECTION
		_nearVehiclesFront = _nearVehicles select { [getpos _veh, getdir _veh, 40, getpos _x] call BIS_fnc_inAngleSector };
		
		//the 1000 is just in case list is empty
		
		_nearVehiclesFront= _nearVehiclesFront apply {_x distance _veh};
		_nearVehiclesFront= _nearVehiclesFront + [1000];
		
		_frontMaxSpeed = _maxSpeed * linearConversion [5, 200, selectMin _nearVehiclesFront, 0, 1, true];
		
		_currentMaxSpeed = selectMin [_densityMaxSpeed, _allRoundMaxSpeed, _frontMaxSpeed];
		
		if (_veh getVariable ["TOV_manually_controlled", False]) then {
			sleep 1;
			continue;
		};
		
		//5 before
		if (_currentMaxSpeed < 2) then {
			_veh limitSpeed 0;
			driver _veh disableAI "MOVE";
						
			//_veh forceSpeed 0;
		} else  {
			_veh limitSpeed _currentMaxSpeed;
			driver _veh enableAI "MOVE";
			
			//_veh forceSpeed _currentMaxSpeed;
			
		};
		
		//Honk management
		if ((_currentMaxSpeed < 5) && !([_veh, 25] call CBA_fnc_nearPlayer) && (_veh getVariable ["TOV_can_honk", True])) then {
			[_veh] spawn TOV_honk_manager;
		};
		
		sleep 0.5;
	};
};

TOV_honk_manager = {
	//Honks the horn, bip bip
	//can only honk once in 60 seconds
	params ["_veh", ["_honk_proba", 0.5], ["_honk_wait", 30]];
	
	if !(_veh getVariable ["TOV_can_honk", True]) exitWith {False};
	
	_veh setVariable ["TOV_can_honk", False, True];
	
	if (random 1 < _honk_proba) then {
		//honk
		_horn = weapons _veh select {["horn",_x] call BIS_fnc_inString};
		_horn resize [1,""];
		_horn = _horn#0;
		
		[_veh, _horn] call BIS_fnc_fire;
		
		sleep 1;
	} else {
		sleep random _honk_wait;
	};
	
	_veh setVariable ["TOV_can_honk", True, True];	
};

TOV_populate_car = {
	params ["_veh", "_eligble_civs"];

	_driver = (createGroup [civilian, true]) createUnit [selectRandom _eligble_civs, _currentSpawnPoint,[],0,"NONE"];  
	_driver moveInDriver _veh;
	
	//Seting up movement
	_driver setBehaviour "CARELESS";
	_driver setSpeedMode "LIMITED";
	
	//Cargo	
	_available_seats=_veh emptyPositions "cargo";
	_passengers=random [0,_available_seats/3,_available_seats];
	for "_i" from 0 to _passengers do {
		_civ = selectRandom _eligble_civs;
		_civ=createAgent [_civ, getpos _driver, [], 0, 'NONE'];
		//_civ = (group _driver) createUnit [_civ, getpos _driver, [],0,"NONE"];
		doStop _civ;
		//_civ assignAsCargo vehicle _driver;
		//_civ disableAI "ALL" ;
		//{_civ enableAI _x} forEach ["ANIM", "MOVE", "PATH", "LIGHTS"];
		
		_civ moveInCargo vehicle _driver;
		_civ setVariable ["TOV_assigned_Vehicle", vehicle _driver, True];
		sleep 0.05;
	};
	
	_driver;
};

