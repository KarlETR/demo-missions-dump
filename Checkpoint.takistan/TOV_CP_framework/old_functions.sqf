OLD_TOV_follow_road_at_CP = {
	params ["_veh", "_CP_pos", ["_range", 150]];
	
	sleep 30;
	_veh forceFollowRoad true;
	
	driver _veh disableAI "ALL" ;
	{driver _veh enableAI _x} forEach ["ANIM", "MOVE", "PATH", "LIGHTS"];
	
	waitUntil { sleep 0.5; (!isNull _veh) || (_veh distance _CP_pos < _range)};
    _veh forceFollowRoad true;
	
	waitUntil {sleep 0.5; (!isNull _veh) || (_veh distance _CP_pos > _range)};
	_veh forceFollowRoad false;
	
};

TOV_speed_manager_old= {
	params ["_veh",["_maxSpeed", 40]];
	
	while {!isNull _veh} do {
	
		_nearVehicles = nearestObjects [_veh, ["CAManBase", "Car", "Truck" , "Tank"], 100] - [_veh];
		
		//60° before
		_nearVehicles = _nearVehicles select { [getpos _veh, getdir _veh, 40, getpos _x] call BIS_fnc_inAngleSector };
		
		//the 1000 is just in case list is empty
		
		_nearVehicles= _nearVehicles apply {_x distance _veh};
		_nearVehicles= _nearVehicles + [1000];
		//if (count _nearVehicles==0) then {}
		
		_currentMaxSpeed = _maxSpeed * linearConversion [5, 200, selectMin _nearVehicles, 0, 1, true];
		//_currentMaxSpeed = _maxSpeed * linearConversion [5, 100, _veh distance _nearVehicles#0, 0, 1, true];
		//hint str _currentMaxSpeed;
		
		if (_veh getVariable ["TOV_manually_controlled", False]) then {
			sleep 1;
			continue;
		};
		
		//5 before
		if (_currentMaxSpeed < 1) then {
			_veh limitSpeed 0;
			driver _veh disableAI "MOVE";
						
			//_veh forceSpeed 0;
		} else  {
			_veh limitSpeed _currentMaxSpeed;
			driver _veh enableAI "MOVE";
			
			//_veh forceSpeed _currentMaxSpeed;
			
		};
		
		sleep 0.5;
	};
};


TOV_speed_manager_old_2= {
	params ["_veh",["_maxSpeed", 60]];
	
	while {!isNull _veh} do {
	
		_nearVehicles = nearestObjects [_veh, ["CAManBase", "Car", "Truck" , "Tank"], 100] - [_veh];
		
		// DENSITY SECTION
		//100 few - 500 is town
		_nearDensity = count (nearestTerrainObjects [_veh, [], 100]);
		_densityMaxSpeed = _maxSpeed * linearConversion [100, 600, _nearDensity, 1, 0.4, true];

		//ALLROUND SECTION
		_nearVehiclesAllRound = _nearVehicles select { [getpos _veh, getdir _veh, 270, getpos _x] call BIS_fnc_inAngleSector };

		_nearVehiclesAllRound = _nearVehicles apply { _veh distance _x } ;
		_nearVehiclesAllRound= _nearVehiclesAllRound + [1000];
		_allRoundMaxSpeed = _maxSpeed * linearConversion [5, 100, selectMin _nearVehiclesAllRound, 0.1, 1, true];

		
		/*_nearVehiclesAllRound resize [5,-1];
		_nearVehiclesAllRound=_nearVehiclesAllRound select { _x > -1};

		if (count _nearVehiclesAllRound > 0) then {
			_nearVehiclesAllRound= _nearVehiclesAllRound call BIS_fnc_arithmeticMean; 
		};
		
		_allRoundMaxSpeed = _maxSpeed * linearConversion [10, 50, _nearDensity, 0.2, 1, true];
		*/
		//FRONT SECTION
		_nearVehiclesFront = _nearVehicles select { [getpos _veh, getdir _veh, 40, getpos _x] call BIS_fnc_inAngleSector };
		
		//the 1000 is just in case list is empty
		
		_nearVehiclesFront= _nearVehiclesFront apply {_x distance _veh};
		_nearVehiclesFront= _nearVehiclesFront + [1000];
		//if (count _nearVehicles==0) then {}
		
		_frontMaxSpeed = _maxSpeed * linearConversion [5, 200, selectMin _nearVehiclesFront, 0, 1, true];
		//
		
		_currentMaxSpeed = selectMin [_densityMaxSpeed, _allRoundMaxSpeed, _frontMaxSpeed];
		
		if (_veh getVariable ["TOV_manually_controlled", False]) then {
			sleep 1;
			continue;
		};
		
		//5 before
		if (_currentMaxSpeed < 1) then {
			_veh limitSpeed 0;
			driver _veh disableAI "MOVE";
						
			//_veh forceSpeed 0;
		} else  {
			_veh limitSpeed _currentMaxSpeed;
			driver _veh enableAI "MOVE";
			
			//_veh forceSpeed _currentMaxSpeed;
			
		};
		
		sleep 0.5;
	};
};