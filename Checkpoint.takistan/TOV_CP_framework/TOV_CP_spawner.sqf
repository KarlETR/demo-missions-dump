//Incoming traffic code

//Server
/*
_spawnPoint= [8619.52,1095.51,0];
_destinationPoint=[7885.43,2323.48,0];
_CP_pos=[7917.59,1492.57,0];
_blacklistAreas=[];
*/

params [["_spawnPoint", []], ["_destinationPoint", []], ["_CP_pos", []], ["_blacklistAreas",[]]];


TOV_CP_eligble_cars=["CUP_C_Datsun",
"CUP_O_Hilux_unarmed_TK_CIV",
"CUP_C_Pickup_unarmed_CIV",
"C_Van_01_fuel_F",
"CUP_C_Golf4_random_Civ",
"C_Hatchback_01_F",
"C_Offroad_02_unarmed_F",
"C_Truck_02_fuel_F",
"C_Truck_02_covered_F",
"C_Offroad_01_covered_F",
"CUP_C_Octavia_CIV",
"C_SUV_01_F",
"C_Van_01_transport_F",
"C_Van_01_box_F",
"C_Van_02_vehicle_F",
"C_Van_02_transport_F",
"CUP_C_Lada_TK2_CIV",
"CUP_C_Lada_GreenTK_CIV",
"CUP_C_LR_Transport_CTK",
"CUP_C_V3S_Covered_TKC",
"CUP_C_SUV_TK",
"CUP_C_Volha_Blue_TKCIV",
"CUP_C_Volha_Gray_TKCIV",
"CUP_C_Volha_Limo_TKCIV"];

TOV_CP_eligble_civs= ["CUP_C_TK_Man"] call TOV_find_civs_classnames;

TOV_CP_contraband_ACE=[
	"Land_HumanSkull_F", 
	"Land_HumanSkeleton_F", 
	"Land_Money_F", 
	"O_CargoNet_01_ammo_F", 
	"Land_FoodSacks_01_cargo_brown_F"
];

TOV_CP_contraband=[
	"CUP_item_Money",
	"acc_flashlight"
];

TOV_CP_explosives_ace=[
	"Box_IED_Exp_F"
];

TOV_CP_explosives=[
	"IEDLandSmall_Remote_Mag",
	"IEDUrbanSmall_Remote_Mag",
	"APERSTripMine_Wire_Mag"
];


TOV_ACE_enabled=True;
TOV_enable_hint_conformity=True;
TOV_CP_max_units=100;
TOV_chance_of_fraud=0.01;

a=true;
maxSleepTime=70;
//maxSleepTime=15;
while {a } do {
	//_currentSpawnPoint = [player getRelPos [1100, 0], 1500] call BIS_fnc_nearestRoad;
	//_currentDestinationPoint=[player getRelPos [200, 120], 1500] call BIS_fnc_nearestRoad;
	
	_tov_units_counts = count (allunits select {_x getVariable ["TOV_CP_object", False]} );
	
	_currentSpawnPoint = [_spawnPoint,100] call BIS_fnc_nearestRoad;
	_currentDestinationPoint= [_destinationPoint,100] call BIS_fnc_nearestRoad;
	
	if !(
			(_tov_units_counts > TOV_CP_max_units) ||
			(isNull _currentSpawnPoint) ||
			(isNull _currentDestinationPoint) ||
			({(getPos player) inArea _x} count _blacklistAreas >0) ||
			(count (nearestObjects [_currentSpawnPoint, ["CAManBase", "Car", "Truck" , "Tank"], 15])>0) 
		) then 
		{
			[_currentSpawnPoint, _currentDestinationPoint, _CP_pos] spawn {
				params ["_currentSpawnPoint","_currentDestinationPoint", "_CP_pos"];
				
				//spawning _vehicule
				//Make better sanity check before spawning
				
				waitUntil {sleep 1; count (nearestObjects [_currentSpawnPoint, ["CAManBase", "Car", "Truck" , "Tank"], 15])==0};
				
				_veh = createVehicle [selectRandom TOV_CP_eligble_cars, _currentSpawnPoint, [], 0, "NONE"];
				clearMagazineCargoGlobal _veh;
				_veh setVariable ["TOV_CP_object", True, True];
				
				_veh setDir (_veh getDir _currentDestinationPoint);
				sleep 0.5;
				
				//Spawning people inside vehicle
				_myCrew=[_veh, TOV_CP_eligble_civs] call TOV_populate_car;
				
				{[_x, TOV_chance_of_fraud] call TOV_initialize_unit_info;} forEach crew _veh;
				
				[_veh] call TOV_add_cp_interaction;
				[_veh, _CP_pos] spawn TOV_hint_conformity;
				

				_veh move (getPos _currentDestinationPoint);
								
				//add speed manager
				[_veh] spawn TOV_speed_manager;
				[_veh, _currentSpawnPoint, getPos _currentDestinationPoint, _CP_pos] spawn TOV_follow_road_at_CP;
				
				//Conditions to delete
				_myCrew = crew _veh;
				{_x setVariable ["TOV_CP_object", True, True]} forEach _myCrew;
				
				sleep 60;
				waitUntil {	
					sleep (1+random 5);
					_seenBy = allPlayers select {_x distance _veh < 20 || {(_x distance _veh < 300 && {([vehicle _x,'VIEW'] checkVisibility [eyePos _x, eyePos driver _veh]) > 0.1})}};
					
					(
						(count _seenBy ==0) && 
						(
							(_veh distance _currentDestinationPoint < 150) || 
							//((_veh distance _CP_pos < 150) && (isNull driver _veh)) ||
							(damage _veh>0.8) ||
							(!(isNull driver _veh) && !(alive driver _veh))
						)
					)
				};
				
				_myCrew = (_myCrew select { !captive _x}) + (crew _veh) ;
				{deleteVehicle _x; sleep 0.1;} forEach (_myCrew + [_veh] + (attachedObjects _veh));
			};
		sleep (5 max (random maxSleepTime));

	} else {
		sleep 3;
	};
};

[
	hint format["Checkpoint summary:\n\nChecked Vehicles : %1\nErrors : %2", TOV_checked_vehicles, TOV_checked_vehicles_error]
] remoteExec ["hint", allPlayers select {_x distance _CP_pos < 60}];

