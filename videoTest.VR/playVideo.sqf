[] spawn {
	_video="armaMovie\AC.ogv";
	_sound="videoSound";
	_screen=tv;

	_screen setObjectTexture [0, _video];
	_soundSource = createVehicle ["Land_HelipadEmpty_F", getPosATL _screen,[],0,"CAN_COLLIDE"];

	_playing=[_video,[10, 10]] spawn BIS_fnc_playVideo;
	_soundSource say3D [_sound,50];

	waitUntil {scriptDone _playing;};
	deleteVehicle _soundSource;
	_screen setObjectTexture [0, _video];

};
//To stoop prematurely : use [""] spawn BIS_fnc_playVideo; tv setObjectTexture [0, ""]; 



