class CfgGroups
{
	class EAST
	{
		name = "$STR_EAST";

		class etr_public_mujahideen
		{
			name = "Afghan Mujahideen 80s";

			class Infantry
			{
				name = "Infantry";

				class etr_public_mujahideen_12_men_heavy
				{
					name = "12 Men Group (Heavy Weapons)";
					faction = "etr_public_mujahideen";
					rarityGroup = 0.500000;
					side = 0;

					class Unit0
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_akm";
						rank = "SERGEANT";
						position[]={0, 0, 0};
					};

					class Unit1
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_mosin";
						rank = "PRIVATE";
						position[]={2, 0, 0};
					};

					class Unit2
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_enfield";
						rank = "PRIVATE";
						position[]={4, 0, 0};
					};

					class Unit3
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_m3";
						rank = "PRIVATE";
						position[]={6, 0, 0};
					};

					class Unit4
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_shotgun";
						rank = "PRIVATE";
						position[]={8, 0, 0};
					};

					class Unit5
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_mosin";
						rank = "PRIVATE";
						position[]={10, 0, 0};
					};

					class Unit6
					{
						side = 0;
						vehicle = "etr_public_mujahideen_MG_PKM";
						rank = "PRIVATE";
						position[]={0, -2, 0};
					};

					class Unit7
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_garand";
						rank = "PRIVATE";
						position[]={2, -2, 0};
					};
					class Unit8
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_mosin";
						rank = "PRIVATE";
						position[]={4, -2, 0};
					};
					class Unit9
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_m3";
						rank = "PRIVATE";
						position[]={6, -2, 0};
					};
					class Unit10
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_RPG";
						rank = "PRIVATE";
						position[]={8, -2, 0};
					};
					class Unit11
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_akm";
						rank = "PRIVATE";
						position[]={10, -2, 0};
					};
				};

				class etr_public_mujahideen_12_men_light
				{
					name = "12 Men Group (Light)";
					faction = "etr_public_mujahideen";
					rarityGroup = 0.500000;
					side = 0;

					class Unit0
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_akm";
						rank = "SERGEANT";
						position[]={0, 0, 0};
					};

					class Unit1
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_mosin";
						rank = "PRIVATE";
						position[]={2, 0, 0};
					};

					class Unit2
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_enfield";
						rank = "PRIVATE";
						position[]={4, 0, 0};
					};

					class Unit3
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_m3";
						rank = "PRIVATE";
						position[]={6, 0, 0};
					};

					class Unit4
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_shotgun";
						rank = "PRIVATE";
						position[]={8, 0, 0};
					};

					class Unit5
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_mosin";
						rank = "PRIVATE";
						position[]={10, 0, 0};
					};

					class Unit6
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_enfield";
						rank = "PRIVATE";
						position[]={0, -2, 0};
					};

					class Unit7
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_garand";
						rank = "PRIVATE";
						position[]={2, -2, 0};
					};
					class Unit8
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_mosin";
						rank = "PRIVATE";
						position[]={4, -2, 0};
					};
					class Unit9
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_m3";
						rank = "PRIVATE";
						position[]={6, -2, 0};
					};
					class Unit10
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_mosin";
						rank = "PRIVATE";
						position[]={8, -2, 0};
					};
					class Unit11
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_akm";
						rank = "PRIVATE";
						position[]={10, -2, 0};
					};
				};

				class etr_public_mujahideen_8_men_rpg
				{
					name = "8 Men Group (RPG-7)";
					faction = "etr_public_mujahideen";
					rarityGroup = 0.500000;
					side = 0;

					class Unit0
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_akm";
						rank = "LIEUTENANT";
						position[]={0, 0, 0};
					};

					class Unit1
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_mosin";
						rank = "PRIVATE";
						position[]={3, 0, 0};
					};

					class Unit2
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_enfield";
						rank = "PRIVATE";
						position[]={5, 0, 0};
					};

					class Unit3
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_m3";
						rank = "PRIVATE";
						position[]={7, 0, 0};
					};

					class Unit4
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_mosin";
						rank = "CORPORAL";
						position[]={9, 0, 0};
					};

					class Unit5
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_mosin";
						rank = "PRIVATE";
						position[]={11, 0, 0};
					};

					class Unit6
					{
						side = 0;
						vehicle = "etr_public_mujahideen_rifleman_RPG";
						rank = "PRIVATE";
						position[]={13, 0, 0};
					};

					class Unit7
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_shotgun";
						rank = "PRIVATE";
						position[]={15, 0, 0};
					};
				};

				class etr_public_mujahideen_8_men_pkm
				{
					name = "8 Men Group (PKM)";
					faction = "etr_public_mujahideen";
					rarityGroup = 0.500000;
					side = 0;

					class Unit0
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_akm";
						rank = "SERGEANT";
						position[]={0, 0, 0};
					};

					class Unit1
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_mosin";
						rank = "PRIVATE";
						position[]={3, 0, 0};
					};

					class Unit2
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_enfield";
						rank = "PRIVATE";
						position[]={5, 0, 0};
					};

					class Unit3
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_m3";
						rank = "PRIVATE";
						position[]={7, 0, 0};
					};

					class Unit4
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_shotgun";
						rank = "PRIVATE";
						position[]={9, 0, 0};
					};

					class Unit5
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_mosin";
						rank = "PRIVATE";
						position[]={11, 0, 0};
					};

					class Unit6
					{
						side = 0;
						vehicle = "etr_public_mujahideen_MG_PKM";
						rank = "PRIVATE";
						position[]={13, 0, 0};
					};

					class Unit7
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_mosin";
						rank = "PRIVATE";
						position[]={15, 0, 0};
					};
				};

				class etr_public_mujahideen_6_men_rpg
				{
					name = "6 Men Group (RPG-7)";
					faction = "etr_public_mujahideen";
					rarityGroup = 0.500000;
					side = 0;

					class Unit0
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_mosin";
						rank = "SERGEANT";
						position[]={0, 0, 0};
					};

					class Unit1
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_m3";
						rank = "PRIVATE";
						position[]={3, 0, 0};
					};

					class Unit2
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_mosin";
						rank = "PRIVATE";
						position[]={5, 0, 0};
					};

					class Unit3
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_RPG";
						rank = "PRIVATE";
						position[]={7, 0, 0};
					};

					class Unit4
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_garand";
						rank = "PRIVATE";
						position[]={9, 0, 0};
					};

					class Unit5
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_akm";
						rank = "PRIVATE";
						position[]={11, 0, 0};
					};
				};

				class etr_public_mujahideen_6_men_pkm
				{
					name = "6 Men Group (PKM)";
					faction = "etr_public_mujahideen";
					rarityGroup = 0.500000;
					side = 0;

					class Unit0
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_mosin";
						rank = "SERGEANT";
						position[]={0, 0, 0};
					};

					class Unit1
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_m3";
						rank = "PRIVATE";
						position[]={3, 0, 0};
					};

					class Unit2
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_mosin";
						rank = "PRIVATE";
						position[]={5, 0, 0};
					};

					class Unit3
					{
						side = 0;
						vehicle = "etr_public_mujahideen_MG_PKM";
						rank = "PRIVATE";
						position[]={7, 0, 0};
					};

					class Unit4
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_garand";
						rank = "PRIVATE";
						position[]={9, 0, 0};
					};

					class Unit5
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_akm";
						rank = "PRIVATE";
						position[]={11, 0, 0};
					};
				};

				class etr_public_mujahideen_4_men_rpg
				{
					name = "4 Men Group (RPG-7)";
					faction = "etr_public_mujahideen";
					rarityGroup = 0.500000;
					side = 0;

					class Unit0
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_enfield";
						rank = "SERGEANT";
						position[]={0, 0, 0};
					};

					class Unit1
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_akm";
						rank = "PRIVATE";
						position[]={3, 0, 0};
					};

					class Unit2
					{
						side = 0;
						vehicle = "etr_public_mujahideen_rifleman_RPG";
						rank = "PRIVATE";
						position[]={5, 0, 0};
					};

					class Unit3
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_mosin";
						rank = "PRIVATE";
						position[]={7, 0, 0};
					};
				};

				class etr_public_mujahideen_4_men_pkm
				{
					name = "4 Men Group (PKM)";
					faction = "etr_public_mujahideen";
					rarityGroup = 0.500000;
					side = 0;

					class Unit0
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_mosin";
						rank = "SERGEANT";
						position[]={0, 0, 0};
					};

					class Unit1
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_enfield";
						rank = "PRIVATE";
						position[]={3, 0, 0};
					};

					class Unit2
					{
						side = 0;
						vehicle = "etr_public_mujahideen_MG_PKM";
						rank = "PRIVATE";
						position[]={5, 0, 0};
					};

					class Unit3
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_mosin";
						rank = "PRIVATE";
						position[]={7, 0, 0};
					};
				};

				class etr_public_mujahideen_4_men_light
				{
					name = "4 Men Group (Light)";
					faction = "etr_public_mujahideen";
					rarityGroup = 0.500000;
					side = 0;

					class Unit0
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_mosin";
						rank = "SERGEANT";
						position[]={0, 0, 0};
					};

					class Unit1
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_enfield";
						rank = "PRIVATE";
						position[]={3, 0, 0};
					};

					class Unit2
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_m3";
						rank = "PRIVATE";
						position[]={5, 0, 0};
					};

					class Unit3
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_mosin";
						rank = "PRIVATE";
						position[]={7, 0, 0};
					};
				};

				class etr_public_mujahideen_3_men_light
				{
					name = "3 Men Group (Light)";
					faction = "etr_public_mujahideen";
					rarityGroup = 0.500000;
					side = 0;

					class Unit0
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_mosin";
						rank = "SERGEANT";
						position[]={0, 0, 0};
					};

					class Unit1
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_enfield";
						rank = "PRIVATE";
						position[]={3, 0, 0};
					};

					class Unit2
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_shotgun";
						rank = "PRIVATE";
						position[]={5, 0, 0};
					};
				};

				class etr_public_mujahideen_3_men_akm
				{
					name = "3 M2n Group (AKM)";
					faction = "etr_public_mujahideen";
					rarityGroup = 0.500000;
					side = 0;

					class Unit0
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_mosin";
						rank = "LIEUTENANT";
						position[]={0, 0, 0};
					};

					class Unit1
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_garand";
						rank = "PRIVATE";
						position[]={3, 0, 0};
					};

					class Unit2
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_akm";
						rank = "PRIVATE";
						position[]={5, 0, 0};
					};
				};

				class etr_public_mujahideen_3_men_rpg
				{
					name = "3 Men Group (RPG-7)";
					faction = "etr_public_mujahideen";
					rarityGroup = 0.500000;
					side = 0;

					class Unit0
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_enfield";
						rank = "SERGEANT";
						position[]={0, 0, 0};
					};

					class Unit1
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_m3";
						rank = "PRIVATE";
						position[]={3, 0, 0};
					};

					class Unit2
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_RPG";
						rank = "PRIVATE";
						position[]={5, 0, 0};
					};
				};

				class etr_public_mujahideen_AA
				{
					name = "AA Group";
					faction = "etr_public_mujahideen";
					rarityGroup = 0.500000;
					side = 0;

					class Unit0
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_mosin";
						rank = "SERGEANT";
						position[]={0, 0, 0};
					};

					class Unit1
					{
						side = 0;
						vehicle = "etr_public_mujahideen_fighter_akm";
						rank = "PRIVATE";
						position[]={3, 0, 0};
					};

					class Unit2
					{
						side = 0;
						vehicle = "etr_public_mujahideen_aa";
						rank = "PRIVATE";
						position[]={5, 0, 0};
					};
				};
			};
		};
	};
};
