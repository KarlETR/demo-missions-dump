#define DEFAULTITEMS "ACE_HandFlare_Red","rhs_mag_nspd","ACE_quikclot","ACE_quikclot","ACE_quikclot","ACE_fieldDressing","ACE_fieldDressing","ACE_fieldDressing","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_packingBandage","ACE_elasticBandage","ACE_packingBandage","ACE_Morphine","ACE_Morphine","ACE_tourniquet","ACE_tourniquet","rhs_mag_rgd5","ACE_EarPlugs"
#define DEFAULTLINKEDITEMS "ItemCompass","ItemWatch"

class CfgVehicles {
	// ---------- UNITS ---------- //
	class etr_public_o_unit_base;
	class etr_public_mujahideen_base: etr_public_o_unit_base
	{
		faction = "etr_public_mujahideen";
		genericnames = "TakistaniMen";
		identitytypes[] =
		{
			"LanguagePER_F",
			"Head_TK",
			"G_IRAN_default"
		};
	};

	class etr_public_mujahideen_fighter_mosin: etr_public_mujahideen_base
	{
		scope=2;
		displayName="Fighter (Mosin)";
		author = AUTHOR;
		backpack="";
		uniformClass="UK3CB_TKM_O_U_03_B";
		weapons[]=
		{
			"rhs_weap_m38",
			"Throw",
			"Put"
		};
		magazines[]=
		{
			mag_12(rhsgref_5Rnd_762x54_m38)
		};
		Items[]=
		{
			DEFAULTITEMS
		};
		linkedItems[]=
		{
			DEFAULTLINKEDITEMS,
			"UK3CB_V_Pouch",
			"UK3CB_TKM_O_H_Turban_01_1"
		};
	};

	class etr_public_mujahideen_fighter_shotgun: etr_public_mujahideen_base
	{
		scope=2;
		displayName="Fighter (IzH-18 12 Gauge)";
		author = AUTHOR;
		backpack="";
		uniformClass="UK3CB_TKM_O_U_03_B";
		weapons[]=
		{
			"rhs_weap_Izh18",
			"Throw",
			"Put"
		};
		magazines[]=
		{
			mag_50(rhsgref_1Rnd_00Buck)
		};
		Items[]=
		{
			DEFAULTITEMS
		};
		linkedItems[]=
		{
			DEFAULTLINKEDITEMS,
			"UK3CB_V_Pouch",
			"UK3CB_TKM_O_H_Turban_01_1"
		};
	};

	class etr_public_mujahideen_fighter_enfield: etr_public_mujahideen_base
	{
		scope=2;
		displayName="Fighter (Lee Enfield)";
		author = AUTHOR;
		backpack="";
		uniformClass="UK3CB_TKC_C_U_01";
		weapons[]=
		{
			"UK3CB_Enfield",
			"Throw",
			"Put"
		};
		magazines[]=
		{
			mag_5(UK3CB_Enfield_Mag)
		};
		Items[]=
		{
			DEFAULTITEMS
		};
		linkedItems[]=
		{
			DEFAULTLINKEDITEMS,
			"V_BandollierB_cbr",
			"UK3CB_TKC_H_Turban_03_1"
		};
	};

	class etr_public_mujahideen_fighter_garand: etr_public_mujahideen_base
	{
		scope=2;
		displayName="Fighter (M1 Garand)";
		author = AUTHOR;
		backpack="";
		uniformClass="UK3CB_TKM_B_U_05_C";
		weapons[]=
		{
			"rhs_weap_m1garand_sa43",
			"Throw",
			"Put"
		};
		magazines[]=
		{
			mag_8(rhsgref_8Rnd_762x63_M2B_M1rifle)
		};
		Items[]=
		{
			DEFAULTITEMS
		};
		linkedItems[]=
		{
			DEFAULTLINKEDITEMS,
			"UK3CB_H_Shemag_white",
			"V_BandollierB_blk"
		};
	};

	class etr_public_mujahideen_fighter_m3: etr_public_mujahideen_base
	{
		scope=2;
		displayName="Fighter (M3A1 'Grease Gun')";
		author = AUTHOR;
		backpack="";
		uniformClass="UK3CB_TKM_B_U_05_C";
		weapons[]=
		{
			"rhs_weap_m3a1",
			"Throw",
			"Put"
		};
		magazines[]=
		{
			mag_6(rhsgref_30rnd_1143x23_M1911B_SMG)
		};
		Items[]=
		{
			DEFAULTITEMS
		};
		linkedItems[]=
		{
			DEFAULTLINKEDITEMS,
			"UK3CB_H_Shemag_tan",
			"UK3CB_V_CW_Chestrig_2_Small"
		};
	};

	class etr_public_mujahideen_fighter_akm: etr_public_mujahideen_base
	{
		scope=2;
		displayName="Fighter (AKM)";
		author = AUTHOR;
		backpack="";
		uniformClass="UK3CB_TKM_I_U_05";
		weapons[]=
		{
			"rhs_weap_akm",
			"Throw",
			"Put"
		};
		magazines[]=
		{
			mag_7(rhs_30Rnd_762x39mm_bakelite)
		};
		Items[]=
		{
			DEFAULTITEMS
		};
		linkedItems[]=
		{
			DEFAULTLINKEDITEMS,
			"UK3CB_H_Shemag_tan",
			"UK3CB_V_CW_Chestrig_2_Small"
		};
	};

	class etr_public_mujahideen_MG_PKM: etr_public_mujahideen_base
	{
		scope=2;
		displayName="Machine Gunner (PKM)";
		author = AUTHOR;
		backpack="etr_public_mujahideen_mgpack";
		uniformClass="rhs_uniform_m88_patchless";
		weapons[]=
		{
			"rhs_weap_pkm",
			"Throw",
			"Put"
		};
		magazines[]=
		{
			mag_2(rhs_100Rnd_762x54mmR)
		};
		Items[]=
		{
			DEFAULTITEMS
		};
		linkedItems[]=
		{
			DEFAULTLINKEDITEMS,
			"rhs_6sh46",
			"UK3CB_TKM_B_H_Turban_04_1"
		};
	};

	class etr_public_mujahideen_fighter_RPG: etr_public_mujahideen_base
	{
		scope=2;
		displayName="Fighter (RPG-7)";
		author = AUTHOR;
		backpack="etr_public_mujahideen_atpack";
		uniformClass="UK3CB_TKC_C_U_01_E";
		weapons[]=
		{
			"rhs_weap_m38",
			"rhs_weap_rpg7",
			"Throw",
			"Put"
		};
		magazines[]=
		{
			mag_5(rhsgref_5Rnd_762x54_m38),
			"rhs_rpg7_PG7V_mag"
		};
		Items[]=
		{
			DEFAULTITEMS
		};
		linkedItems[]=
		{
			DEFAULTLINKEDITEMS,
			"UK3CB_H_Worker_Cap_01",
			"UK3CB_V_Pouch"
		};
	};

	class etr_public_mujahideen_officer: etr_public_mujahideen_base
	{
		scope=2;
		displayName="Officer";
		author = AUTHOR;
		backpack="";
		uniformClass="UK3CB_TKA_I_U_CombatUniform_03_TKA_Brush";
		weapons[]=
		{
			"rhs_weap_makarov_pm",
			"Throw",
			"Put"
		};
		magazines[]=
		{
			mag_4(rhs_mag_9x18_8_57N181S)
		};
		Items[]=
		{
			DEFAULTITEMS
		};
		linkedItems[]=
		{
			"UK3CB_H_Civ_Beret",
			"ItemMap",
			"ItemCompass",
			"ItemWatch",
			"ItemRadio"
		};
	};

	class etr_public_mujahideen_aa: etr_public_mujahideen_base
	{
		scope=2;
		displayName="Anti-Air (9K38 Igla)";
		author = AUTHOR;
		backpack="etr_public_mujahideen_aapack";
		uniformClass="UK3CB_TKC_C_U_06_C";
		weapons[]=
		{
			"rhs_weap_m38",
			"rhs_weap_igla",
			"Throw",
			"Put"
		};
		magazines[]=
		{
			mag_2(rhs_mag_9k38_rocket),
			mag_5(rhsgref_5Rnd_762x54_m38)
		};
		Items[]=
		{
			DEFAULTITEMS
		};
		linkedItems[]=
		{
			DEFAULTLINKEDITEMS,
			"V_BandollierB_blk",
			"UK3CB_H_Worker_Cap_01"
		};
	};

//////// Vehicles ////////

	class UK3CB_TKM_O_BTR60;
	class etr_public_mujahideen_btr60: UK3CB_TKM_O_BTR60
	{
		scope = 2;
		side = 0;
		faction = "etr_public_mujahideen";
		crew = "etr_public_mujahideen_fighter_mosin","etr_public_mujahideen_fighter_m3";
		typicalCargo[]={"etr_public_mujahideen_fighter_mosin", "etr_public_mujahideen_fighter_m3", "etr_public_mujahideen_fighter_mosin"};
		author = AUTHOR;
		transportMaxWeapons = 20;
		transportMaxMagazines = 300;
		transportmaxbackpacks = 4;

		class TransportWeapons {};
		class TransportItems {};
		class TransportMagazines {};
	};

	class UK3CB_TKM_O_BTR40_MG;
	class etr_public_mujahideen_btr40: UK3CB_TKM_O_BTR40_MG
	{
		scope = 2;
		side = 0;
		faction = "etr_public_mujahideen";
		crew = "etr_public_mujahideen_fighter_mosin","etr_public_mujahideen_fighter_m3";
		typicalCargo[]={"etr_public_mujahideen_fighter_mosin", "etr_public_mujahideen_fighter_m3", "etr_public_mujahideen_fighter_mosin"};
		author = AUTHOR;
		transportMaxWeapons = 20;
		transportMaxMagazines = 300;
		transportmaxbackpacks = 4;

		class TransportWeapons {};
		class TransportItems {};
		class TransportMagazines {};
	};

	class UK3CB_TKM_O_V3S_Zu23;
	class etr_public_mujahideen_prada_zu23: UK3CB_TKM_O_V3S_Zu23
	{
		scope = 2;
		side = 0;
		faction = "etr_public_mujahideen";
		crew = "etr_public_mujahideen_fighter_mosin","etr_public_mujahideen_fighter_m3";
		typicalCargo[]={"etr_public_mujahideen_fighter_mosin", "etr_public_mujahideen_fighter_m3", "etr_public_mujahideen_fighter_mosin","etr_public_mujahideen_fighter_mosin"};
		author = AUTHOR;
		transportMaxWeapons = 20;
		transportMaxMagazines = 300;
		transportmaxbackpacks = 4;

		class TransportWeapons {};
		class TransportItems {};
		class TransportMagazines {};
	};

	class UK3CB_TKM_O_Ural_Zu23;
	class etr_public_mujahideen_ural_zu23: UK3CB_TKM_O_Ural_Zu23
	{
		scope = 2;
		side = 0;
		faction = "etr_public_mujahideen";
		crew = "etr_public_mujahideen_fighter_mosin","etr_public_mujahideen_fighter_m3";
		typicalCargo[]={"etr_public_mujahideen_fighter_mosin", "etr_public_mujahideen_fighter_m3", "etr_public_mujahideen_fighter_mosin","etr_public_mujahideen_fighter_mosin"};
		author = AUTHOR;
		transportMaxWeapons = 20;
		transportMaxMagazines = 300;
		transportmaxbackpacks = 4;

		class TransportWeapons {};
		class TransportItems {};
		class TransportMagazines {};
	};

	class UK3CB_TKM_O_UAZ_Dshkm;
	class etr_public_mujahideen_rhsgref_uaz_dshkm: UK3CB_TKM_O_UAZ_Dshkm
	{
		scope = 2;
		side = 0;
		faction = "etr_public_mujahideen";
		crew = "etr_public_mujahideen_fighter_mosin","etr_public_mujahideen_fighter_m3";
		typicalCargo[]={"etr_public_mujahideen_fighter_mosin", "etr_public_mujahideen_fighter_m3", "etr_public_mujahideen_fighter_mosin"};
		author = AUTHOR;
		transportMaxWeapons = 20;
		transportMaxMagazines = 300;
		transportmaxbackpacks = 4;

		class TransportWeapons {};
		class TransportItems {};
		class TransportMagazines {};
	};

	class UK3CB_TKM_O_UAZ_AGS30;
	class etr_public_mujahideen_rhsgref_uaz_dshkm: UK3CB_TKM_O_UAZ_AGS30
	{
		scope = 2;
		side = 0;
		faction = "etr_public_mujahideen";
		crew = "etr_public_mujahideen_fighter_mosin","etr_public_mujahideen_fighter_m3";
		typicalCargo[]={"etr_public_mujahideen_fighter_mosin", "etr_public_mujahideen_fighter_m3", "etr_public_mujahideen_fighter_mosin"};
		author = AUTHOR;
		transportMaxWeapons = 20;
		transportMaxMagazines = 300;
		transportmaxbackpacks = 4;

		class TransportWeapons {};
		class TransportItems {};
		class TransportMagazines {};
	};

	class UK3CB_TKM_O_UAZ_SPG9;
	class etr_public_mujahideen_rhsgref_uaz_dshkm: UK3CB_TKM_O_UAZ_SPG9
	{
		scope = 2;
		side = 0;
		faction = "etr_public_mujahideen";
		crew = "etr_public_mujahideen_fighter_mosin","etr_public_mujahideen_fighter_m3";
		typicalCargo[]={"etr_public_mujahideen_fighter_mosin", "etr_public_mujahideen_fighter_m3", "etr_public_mujahideen_fighter_mosin"};
		author = AUTHOR;
		transportMaxWeapons = 20;
		transportMaxMagazines = 300;
		transportmaxbackpacks = 4;

		class TransportWeapons {};
		class TransportItems {};
		class TransportMagazines {};
	};

	class UK3CB_TKM_O_LR_M2;
	class etr_public_mujahideen_rhsgref_uaz_dshkm: UK3CB_TKM_O_LR_M2
	{
		scope = 2;
		side = 0;
		faction = "etr_public_mujahideen";
		crew = "etr_public_mujahideen_fighter_mosin","etr_public_mujahideen_fighter_m3";
		typicalCargo[]={"etr_public_mujahideen_fighter_mosin", "etr_public_mujahideen_fighter_m3", "etr_public_mujahideen_fighter_mosin"};
		author = AUTHOR;
		transportMaxWeapons = 20;
		transportMaxMagazines = 300;
		transportmaxbackpacks = 4;

		class TransportWeapons {};
		class TransportItems {};
		class TransportMagazines {};
	};

	class UK3CB_TKM_O_LR_AGS30;
	class etr_public_mujahideen_rhsgref_uaz_dshkm: UK3CB_TKM_O_LR_AGS30
	{
		scope = 2;
		side = 0;
		faction = "etr_public_mujahideen";
		crew = "etr_public_mujahideen_fighter_mosin","etr_public_mujahideen_fighter_m3";
		typicalCargo[]={"etr_public_mujahideen_fighter_mosin", "etr_public_mujahideen_fighter_m3", "etr_public_mujahideen_fighter_mosin"};
		author = AUTHOR;
		transportMaxWeapons = 20;
		transportMaxMagazines = 300;
		transportmaxbackpacks = 4;

		class TransportWeapons {};
		class TransportItems {};
		class TransportMagazines {};
	};

	class UK3CB_TKM_O_LR_SPG9;
	class etr_public_mujahideen_rhsgref_uaz_dshkm: UK3CB_TKM_O_LR_SPG9
	{
		scope = 2;
		side = 0;
		faction = "etr_public_mujahideen";
		crew = "etr_public_mujahideen_fighter_mosin","etr_public_mujahideen_fighter_m3";
		typicalCargo[]={"etr_public_mujahideen_fighter_mosin", "etr_public_mujahideen_fighter_m3", "etr_public_mujahideen_fighter_mosin"};
		author = AUTHOR;
		transportMaxWeapons = 20;
		transportMaxMagazines = 300;
		transportmaxbackpacks = 4;

		class TransportWeapons {};
		class TransportItems {};
		class TransportMagazines {};
	};

	class UK3CB_TKM_O_T34;
	class etr_public_mujahideen_rhsgref_BRDM2_HQ_msv: UK3CB_TKM_O_T34
	{
		scope = 2;
		side = 0;
		faction = "etr_public_mujahideen";
		crew = "etr_public_mujahideen_fighter_mosin","etr_public_mujahideen_fighter_m3","etr_public_mujahideen_fighter_enfield";
		typicalCargo[]={"etr_public_mujahideen_fighter_mosin", "etr_public_mujahideen_fighter_m3", "etr_public_mujahideen_fighter_mosin","etr_public_mujahideen_fighter_enfield"};
		author = AUTHOR;
		transportMaxWeapons = 20;
		transportMaxMagazines = 300;
		transportmaxbackpacks = 4;

		class TransportWeapons {};
		class TransportItems {};
		class TransportMagazines {};
	};

	class UK3CB_TKM_I_BMP1;
	class etr_public_mujahideen_bmp1: UK3CB_TKM_I_BMP1
	{
		scope = 2;
		side = 0;
		faction = "etr_public_mujahideen";
		crew = "etr_public_mujahideen_fighter_mosin","etr_public_mujahideen_fighter_m3";
		typicalCargo[]={"etr_public_mujahideen_fighter_mosin", "etr_public_mujahideen_fighter_m3", "etr_public_mujahideen_fighter_mosin"};
		author = AUTHOR;
		transportMaxWeapons = 20;
		transportMaxMagazines = 300;
		transportmaxbackpacks = 4;

		class TransportWeapons {};
		class TransportItems {};
		class TransportMagazines {};
	};

	// ---------- EQUIPMENT ---------- //
	class rhs_rpg_empty;
	class etr_public_mujahideen_aapack: rhs_rpg_empty {
		maximumLoad=120;
		class TransportItems {
			TransItem(rhs_mag_9k38_rocket,2)
		};
	};
	class etr_public_mujahideen_atpack: rhs_rpg_empty {
		maximumLoad=120;
		class TransportItems {
			TransItem(rhs_rpg7_PG7V_mag,3)
		};
	};
	class UK3CB_TKM_I_B_RIF;
	class etr_public_mujahideen_mgpack: UK3CB_TKM_I_B_RIF {
		maximumLoad=120;
		class TransportItems {
			TransItem(rhs_100Rnd_762x54mmR,3)
		};
	};
};

class Extended_Init_Eventhandlers {
	class etr_public_mujahideen_base {
		serverInit="(_this select 0) execVM 'etr_public_mujahideen\functions\loadout.sqf'";
	};
	class etr_public_mujahideen_officer {
		serverInit="";
	};
};
