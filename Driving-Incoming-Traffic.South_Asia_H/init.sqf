//player vehicle management
playerMaxSpeed=90;
frontVehicle=vehicle leader aa;

player setAnimSpeedCoef 0;
player addItem "ACE_microDAGR";
vehicle player limitSpeed playerMaxSpeed;
vehicle player setBehaviour "CARELESS";
frontVehicle setBehaviour "CARELESS";

setViewDistance 40000;
{_x allowDamage false;(vehicle _x) allowDamage false;} forEach allUnits;
setDate [2012,6,4,18,38];
//Incoming traffic code
[] spawn {

	cars=["CUP_C_Datsun",
	"C_Van_01_fuel_F",
	"CUP_C_Golf4_random_Civ",
	"C_Hatchback_01_F",
	"C_Offroad_02_unarmed_F",
	"C_Truck_02_fuel_F",
	"C_Truck_02_covered_F",
	"C_Offroad_01_covered_F",
	"CUP_C_Octavia_CIV",
	"C_SUV_01_F",
	"C_Van_01_transport_F",
	"C_Van_01_box_F",
	"C_Van_02_vehicle_F",
	"C_Van_02_transport_F",
	"CUP_C_Lada_TK2_CIV",
	"CUP_C_Lada_GreenTK_CIV",
	"CUP_C_LR_Transport_CTK",
	"CUP_C_V3S_Covered_TKC",
	"CUP_C_SUV_TK",
	"CUP_C_Volha_Blue_TKCIV",
	"CUP_C_Volha_Gray_TKCIV",
	"CUP_C_Volha_Limo_TKCIV"];

	a=true;
	maxSleepTime=30;
	blacklistAreas=["blacklist_1","blacklist_2"];
	//maxSleepTime=15;
	while {a } do {
		_spawnPos = [player getRelPos [1100, 0], 1500] call BIS_fnc_nearestRoad;
		_destination=[player getRelPos [200, 120], 1500] call BIS_fnc_nearestRoad;
		
		if !( (isNull _spawnPos) || (isNull _destination) || ({(getPos player) inArea _x} count blacklistAreas >0)) then {
			[_spawnPos,_destination] spawn {
				params ["_spawnPos","_destination"];
				_veh = createVehicle [selectRandom cars, _spawnPos, [], 0, "NONE"];
				_veh setDir (_veh getDir player);
				sleep 0.5;
				_myCrew=createVehicleCrew _veh;

				_veh move (getPos _destination);
				_myCrew setBehaviour "CARELESS";
				_myCrew setSpeedMode "LIMITED";
				_veh limitSpeed 70;
				sleep 60;
				waitUntil {	
					sleep (1+random 5); 
					_check=[getPos player, getDir player, 180, getPos _veh] call BIS_fnc_inAngleSector;
					((!_check && (player distance _veh > 200)) || player distance _veh > 1000) };
				{deleteVehicle _x;sleep 0.1;} forEach (units _myCrew) + [_veh];
			};
			sleep (5 max (random maxSleepTime));
		} else {
			sleep 3;
		};
	};

};


//Speed management of front vehicle
[] spawn {
	speedManagement=true;
	frontVehicleMaxSpeed=playerMaxSpeed;
	frontVehicle limitSpeed frontVehicleMaxSpeed;
	while {speedManagement} do {
		sleep 5;
		{_x setFuel 1;} forEach [frontVehicle,vehicle player];
		if (player distance frontVehicle > 500) then {
			frontVehicle limitSpeed frontVehicleMaxSpeed*0.7
		} else {
			if (player distance frontVehicle < 100) then { frontVehicle limitSpeed frontVehicleMaxSpeed };
		};
	};
};

hideRoadClutter2={
	params ["_center","_radius"];
	{
		{
			hideObject _x;
		} forEach nearestTerrainObjects [getpos _x, [], 18, false];
	} forEach (_center nearRoads _radius);
};

[[(worldSize / 2), (worldSize / 2), 0], (sqrt 2 * (worldSize / 2))] call hideRoadClutter2;

//Interaction for speed management
currentMaxSpeed = 90;
private _self_maxSpeed = ['maxSpeed','Limit speed to ','',{hintSilent format["Current max speed: %1", currentMaxSpeed];},{vehicle player != player}] call ace_interact_menu_fnc_createAction;
[player, 1, ["ACE_SelfActions"], _self_maxSpeed] call ace_interact_menu_fnc_addActionToObject;

private _self_speedValue = ['noLimit','Limitless','',{currentMaxSpeed = "No limit"; (vehicle player) limitSpeed -1;},{!("No limit" isEqualTo currentMaxSpeed)}] call ace_interact_menu_fnc_createAction;
[player, 1, ["ACE_SelfActions", "maxSpeed"], _self_speedValue] call ace_interact_menu_fnc_addActionToObject;

{
	private _self_speedValue = [format["view%1",_x],format["%1",_x],'',compile format["currentMaxSpeed=%1;(vehicle player) limitSpeed %1;", _x],compile format["!(%1 isEqualTo currentMaxSpeed)",_x]] call ace_interact_menu_fnc_createAction;
	[player, 1, ["ACE_SelfActions", "maxSpeed"], _self_speedValue] call ace_interact_menu_fnc_addActionToObject;
} forEach [30,50,70,80,90,120];
